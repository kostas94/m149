package com.sample.postgress;

import java.io.File;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.Scanner;  
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.sample.postgress.bean.*;
import com.sample.postgress.service.*;
import java.util.Locale;
import java.util.ArrayList;
import java.sql.*;

@SpringBootApplication(scanBasePackages="com.sample")
public class PostgressApplication {

	@Resource 
	AccessService accessService;
	@Resource 
	DataXceiverService dataXceiverService;
	@Resource 
	FS_NamesystemService fS_NamesystemService;

	

	private void create_indexes()
	{
		String createFunction = 
			" create index timestamp_index on common_fields(timestamp); "			+
			" create index refreq_index on accesslog(referer, resourcerequested); "	+
			" create index ip_index on common_fields(ipaddress); " 					+
			" create index accesslog_size_index on accesslog(responsesize); ";
		
		try (Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/LogDB", "postgres", "Fall19");
             Statement statement = conn.createStatement()){
            statement.execute(createFunction);
			conn.close();	
		}
		catch (SQLException e) {
			System.out.println("Problem in creation of the indexes");
		 		e.printStackTrace();
		}
	}

	
	
	
	private void create_function_1()
	{
		String createFunction = 
			"CREATE OR REPLACE FUNCTION Question1 (from_date TIMESTAMP, until_date TIMESTAMP) " + 
			"   RETURNS TABLE ( " 																+
			"      type VARCHAR, " 																+
			"      total_logs INT " 															+
			"   ) "																				+ 
			" AS $$ " 																			+
			" BEGIN " 																			+
			"   RETURN QUERY SELECT " 															+
			"		cf.type, " 																	+
			"		COUNT(*)::int as total_logs " 												+
			"	FROM " 																			+
			"		common_fields AS cf " 														+
			"	WHERE " 																		+
			"		cf.timestamp >= from_date AND cf.timestamp <= until_date " 					+
			"	GROUP BY " 																		+
			"		cf.type " 																	+
			"	ORDER BY " 																		+
			"		total_logs DESC; " 															+
			" END; $$ "  																		+
			" LANGUAGE 'plpgsql'; ";
		
		try (Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/LogDB", "postgres", "Fall19");
             Statement statement = conn.createStatement()){
            statement.execute(createFunction);
			conn.close();	
		}
		catch (SQLException e) {
			System.out.println("Problem in creation of the first Function");
		}
	}

	

	private void create_function_2()
	{
		String createFunction = 
			"CREATE OR REPLACE FUNCTION Question2 (from_date TIMESTAMP, until_date TIMESTAMP, action_type VARCHAR) " 	+ 
			"   RETURNS TABLE ( " 																						+
			"      day_of_time_range date, " 																			+
			"      total_logs INT " 																					+
			"   ) "																										+ 
			" AS $$ " 																									+
			" BEGIN " 																									+
			"   RETURN QUERY SELECT " 																					+
			"		date(cf.timestamp) as day_of_time_range, "															+
			"		COUNT(cf.type)::int as total_logs " 																+
			"	FROM " 																									+
			"		common_fields AS cf " 																				+
			"	WHERE " 																								+
			"		cf.timestamp >= from_date AND cf.timestamp <= until_date AND cf.type = action_type " 				+
			"	GROUP BY " 																								+
			"		day_of_time_range; " 																				+
			" END; $$ "  																								+
			" LANGUAGE 'plpgsql'; ";
		
		try (Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/LogDB", "postgres", "Fall19");
             Statement statement = conn.createStatement()){
            statement.execute(createFunction);
			conn.close();	
		}
		catch (SQLException e) {
			System.out.println("Problem in creation of the second Function");
		}
	}
	

	private void create_function_3()
	{
		String createFunction = 
			"CREATE OR REPLACE FUNCTION Question3 (specific_day TIMESTAMP) " 														+ 
			"   RETURNS TABLE ( " 																									+
			"      ipAddress VARCHAR, " 																							+
			"      most_common_log_type VARCHAR " 																					+
			"   ) "																													+ 
			" AS $$ " 																												+
			" BEGIN " 																												+
			"   RETURN QUERY SELECT " 																								+
			"		from2.ipaddress, type "																							+
			"	FROM " 																												+
			" 		(SELECT "																										+	
			"			from1.ipaddress, from1.type, ROW_NUMBER() OVER (PARTITION BY from1.ipaddress ORDER BY freq DESC) AS rn "	+
			"		FROM " 																											+
	 		"			(SELECT " 																									+
			"				cf.ipaddress, cf.type, COUNT('x') AS freq " 															+
			"			FROM common_fields as cf " 																					+
			" 			WHERE date(cf.timestamp)=date(specific_day) " 																+
			"			GROUP BY 1, 2 " 																							+
			" 			) as from1 " 																								+
			"		) as from2 " 																									+
			" 	WHERE rn = 1; " 																									+
			" END; $$ "  																											+
			" LANGUAGE 'plpgsql'; ";
		
		try (Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/LogDB", "postgres", "Fall19");
             Statement statement = conn.createStatement()){
            statement.execute(createFunction);
			conn.close();	
		}
		catch (SQLException e) {
			System.out.println("Problem in creation of the third Function");
		}
	}
	
	
	private void create_function_4()
	{
		String createFunction = 
			"CREATE OR REPLACE FUNCTION Question4 (from_date TIMESTAMP, until_date TIMESTAMP) " + 
			"   RETURNS TABLE ( " 																+
			"      block_id VARCHAR " 															+
			"   ) "																				+ 
			" AS $$ " 																			+
			" BEGIN " 																			+
			"   RETURN QUERY SELECT " 															+
			"		b.block_id "																+
			"	FROM " 																			+
			" 		common_fields AS cf NATURAL JOIN blocks AS b "								+	
			" 	WHERE " 																		+
			" 		cf.timestamp>=from_date AND cf.timestamp<=until_date " 						+
			" 	GROUP BY " 																		+
			" 		b.block_id " 																+
			" 	ORDER BY " 																		+
			" 		(CAST(COUNT(type) AS FLOAT)/(until_date::date -from_date::date)) DESC " 	+
			" 	LIMIT 5; " 																		+
			" END; $$ "  																		+
			" LANGUAGE 'plpgsql'; ";
		
		try (Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/LogDB", "postgres", "Fall19");
             Statement statement = conn.createStatement()){
            statement.execute(createFunction);
			conn.close();	
		}
		catch (SQLException e) {
			System.out.println("Problem in creation of the fourth Function");
		}
	}

	
	
	private void create_function_5()
	{
		String createFunction = 
			"CREATE OR REPLACE FUNCTION Question5 () " + 
			"   RETURNS TABLE ( " 																+
			"      referer VARCHAR " 															+
			"   ) "																				+ 
			" AS $$ " 																			+
			" BEGIN " 																			+
			"   RETURN QUERY SELECT " 															+
			"		acc.referer " 																+
			"	FROM " 																			+
			"		accesslog AS acc " 															+
			"	GROUP BY " 																		+
			"		acc.referer " 																+
			"	HAVING " 																		+
			"		COUNT(DISTINCT acc.resourcerequested) > 1; " 								+
			" END; $$ "  																		+
			" LANGUAGE 'plpgsql'; ";
		
		try (Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/LogDB", "postgres", "Fall19");
             Statement statement = conn.createStatement()){
            statement.execute(createFunction);
			conn.close();	
		}
		catch (SQLException e) {
			System.out.println("Problem in creation of the fifth Function");
		}
	}

	
	
	
	
	private void create_function_6()
	{
		String createFunction = 
				"CREATE OR REPLACE FUNCTION Question6 () " 				+ 
				"   RETURNS TABLE ( " 									+
				"      Second_Most_Common_Resource_Requested VARCHAR " 	+
				"   ) "													+ 
				" AS $$ "	 											+
				" BEGIN " 												+
				"   RETURN QUERY SELECT " 								+
				"		acc.resourcerequested "							+
				"	FROM "												+
				" 		accesslog as acc "								+	
				"	GROUP BY "											+
				"		acc.resourcerequested "							+
				" 	ORDER BY "											+
				"		COUNT(resourcerequested) DESC " 				+
				" OFFSET 1 "											+
				" LIMIT 1; "											+
				" END; $$ "  											+
				" LANGUAGE 'plpgsql'; ";
		try (Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/LogDB", "postgres", "Fall19");
	             Statement statement = conn.createStatement()){
	            statement.execute(createFunction);
				conn.close();	
			}
			catch (SQLException e) {
				System.out.println("Problem in creation of the sixth Function");
			}
	}

	
	
	private void create_function_7()
	{
		String createFunction = 
			"CREATE OR REPLACE FUNCTION Question7 (maximum_size INTEGER) " + 
			"   RETURNS TABLE ( " 																																								+
			"      id INTEGER, httpmethod VARCHAR, httpresponsestatus INTEGER, referer VARCHAR, resourcerequested VARCHAR, "						+
			" 		responsesize INTEGER, useragent VARCHAR, userid VARCHAR, ipaddress VARCHAR, time_stamp timestamp with time zone, type VARCHAR" 	+
			"   ) "																																	+ 
			" AS $$ " 																																+
			" BEGIN "																																+
			"   RETURN QUERY SELECT " 																												+
			"		acc.id,	acc.httpmethod, acc.httpresponsestatus, acc.referer, acc.resourcerequested, acc.responsesize,"							+ 
			"	acc.useragentstring, acc.userid, acc.ipaddress, acc.timestamp, acc.type " 															+
			"	FROM " 																																+
			"		(accesslog NATURAL JOIN common_fields) AS acc " 																				+
			"	WHERE " 																															+
			"		acc.responsesize<=maximum_size AND acc.responsesize >= 0; " 																	+
			" END; $$ "  																															+
			" LANGUAGE 'plpgsql'; ";
		
		try (Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/LogDB", "postgres", "Fall19");
             Statement statement = conn.createStatement()){
            statement.execute(createFunction);
			conn.close();	
		}
		catch (SQLException e) {
			System.out.println("Problem in creation of the seventh Function");
		}
	}
	

	
	private void create_function_8()
	{
		String createFunction = 
			"CREATE OR REPLACE FUNCTION Question8 () " 															+
			"   RETURNS TABLE ( " 																				+
			"      block_id VARCHAR" 																			+
			"   ) "																								+ 
			" AS $$ " 																							+
			" BEGIN "																							+
			"   RETURN QUERY SELECT " 																			+
			"		DISTINCT b1.block_id " 																		+
			"	FROM " 																							+
			"		blocks AS b1, blocks AS b2, common_fields AS cf1, common_fields AS cf2  " 					+
			"	WHERE " 																						+
			"		b1.key_id != b2.key_id AND b1.block_id=b2.block_id AND cf1.id=b1.id AND cf2.id=b2.id"		+ 
			" AND cf1.type='replicate' AND cf2.type='Served' AND date(cf1.timestamp) = date(cf2.timestamp); " 	+
			" END; $$ "  																						+
			" LANGUAGE 'plpgsql'; ";
		
		try (Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/LogDB", "postgres", "Fall19");
             Statement statement = conn.createStatement()){
            statement.execute(createFunction);
			conn.close();	
		}
		catch (SQLException e) {
			System.out.println("Problem in creation of the eighth Function");
		}
	}
	
	

	private void create_function_9()
	{
		String createFunction = 
			"CREATE OR REPLACE FUNCTION Question9 () " 																				+
			"   RETURNS TABLE ( " 																									+
			"      block_id VARCHAR"					 																			+
			"   ) "																													+ 
			" AS $$ " 																												+
			" BEGIN "																												+
			"   RETURN QUERY SELECT " 																								+
			"		DISTINCT b1.block_id " 																							+
			"	FROM " 																												+
			"		blocks AS b1, blocks AS b2, common_fields AS cf1, common_fields AS cf2  " 										+
			"	WHERE " 																											+
			"		b1.key_id != b2.key_id AND b1.block_id=b2.block_id AND cf1.id=b1.id AND cf2.id=b2.id AND cf1.type='replicate'" 	+
			" AND cf2.type='Served' AND DATE_TRUNC('hour', cf1.timestamp) = DATE_TRUNC('hour', cf2.timestamp); " 					+
			" END; $$ "  																											+
			" LANGUAGE 'plpgsql'; ";
		
		try (Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/LogDB", "postgres", "Fall19");
             Statement statement = conn.createStatement()){
            statement.execute(createFunction);
			conn.close();	
		}
		catch (SQLException e) {
			System.out.println("Problem in creation of the ninth Function");
		}
	}


	private void create_function_10()
	{
		String createFunction = 
			"CREATE OR REPLACE FUNCTION Question10 (version VARCHAR) " + 
			"   RETURNS TABLE ( " 																																								+
			"      id INTEGER, httpmethod VARCHAR, httpresponsestatus INTEGER, referer VARCHAR, resourcerequested VARCHAR, "						+
			" 		responsesize INTEGER, useragent VARCHAR, userid VARCHAR, ipaddress VARCHAR, time_stamp timestamp with time zone, type VARCHAR" 	+
			"   ) "																																	+ 
			" AS $$ " 																																+
			" BEGIN "																																+
			"   RETURN QUERY SELECT " 																												+
			"		acc.id, acc.httpmethod, acc.httpresponsestatus, acc.referer, acc.resourcerequested, acc.responsesize,"							+ 
			"	acc.useragentstring, acc.userid, acc.ipaddress, acc.timestamp, acc.type " 															+
			"	FROM " 																																+
			"		(accesslog NATURAL JOIN common_fields) AS acc " 																				+
			"	WHERE " 																															+
			"		useragentstring LIKE '%Mozilla/' || version || '%'; " 																			+
			" END; $$ "  																															+
			" LANGUAGE 'plpgsql'; ";
		
		try (Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/LogDB", "postgres", "Fall19");
             Statement statement = conn.createStatement()){
            statement.execute(createFunction);
			conn.close();	
		}
		catch (SQLException e) {
			System.out.println("Problem in creation of the tenth Function");
		}
	}

	

	private void create_function_11()
	{
		String createFunction = 
			"CREATE OR REPLACE FUNCTION Question11 (from_date TIMESTAMP, until_date TIMESTAMP, method VARCHAR) " 				+
			"   RETURNS TABLE ( " 																								+
			"      ip_Address VARCHAR"					 																		+
			"   ) "																												+ 
			" AS $$ " 																											+
			" BEGIN "																											+
			"   RETURN QUERY SELECT " 																							+
			"		DISTINCT cf.ipaddress " 																					+
			"	FROM " 																											+
			"		accesslog AS acc NATURAL JOIN common_fields AS cf" 															+
			"	WHERE " 																										+
			"		cf.timestamp>=from_date AND cf.timestamp<=until_date AND acc.httpmethod=method; " 							+
			" END; $$ "  																										+
			" LANGUAGE 'plpgsql'; ";
		
		try (Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/LogDB", "postgres", "Fall19");
             Statement statement = conn.createStatement()){
            statement.execute(createFunction);
			conn.close();	
		}
		catch (SQLException e) {
			System.out.println("Problem in creation of the eleventh Function");
		}
	}
	

	
	private void create_function_12()
	{
		String createFunction = 
			"CREATE OR REPLACE FUNCTION Question12 (from_date TIMESTAMP, until_date TIMESTAMP, method1 VARCHAR, method2 VARCHAR) " 	+
			"   RETURNS TABLE ( " 																									+
			"      ip_Address VARCHAR"						 																		+
			"   ) "																													+ 
			" AS $$ " 																												+
			" BEGIN "																												+
			"   RETURN QUERY SELECT " 																								+
			"		cf.ipaddress " 																									+
			"	FROM " 																												+
			"		accesslog AS acc NATURAL JOIN common_fields AS cf" 																+
			"	WHERE " 																											+
			"		cf.timestamp>=from_date AND cf.timestamp<=until_date AND acc.httpmethod=method1 " 								+
			"   INTERSECT 	" 																										+
			"   SELECT " 																											+
			"		cf.ipaddress "			 																						+
			"	FROM " 																												+
			"		accesslog AS acc NATURAL JOIN common_fields AS cf" 																+
			"	WHERE " 																											+
			"		cf.timestamp>=from_date AND cf.timestamp<=until_date AND acc.httpmethod=method2; " 								+
			" END; $$ "  																											+
			" LANGUAGE 'plpgsql'; ";
		
		try (Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/LogDB", "postgres", "Fall19");
             Statement statement = conn.createStatement()){
            statement.execute(createFunction);
			conn.close();	
		}
		catch (SQLException e) {
			System.out.println("Problem in creation of the eleventh Function");
		}
	}
	
	
	
	
	private void create_function_13()
	{
		String createFunction = 
			"CREATE OR REPLACE FUNCTION Question13 (from_date TIMESTAMP, until_date TIMESTAMP) "	+
			"   RETURNS TABLE ( " 																	+
			"      ip_Address VARCHAR"					 											+
			"   ) "																					+ 
			" AS $$ " 																				+
			" BEGIN "																				+
			"   RETURN QUERY SELECT " 																+
			"		cf.ipaddress " 																	+
			"	FROM " 																				+
			"		accesslog AS acc NATURAL JOIN common_fields AS cf " 							+
			"	WHERE " 																			+
			"		cf.timestamp>=from_date AND cf.timestamp<=until_date " 							+
			"	GROUP BY "																			+
			"		cf.ipaddress "																	+
			"	HAVING "																			+
			"		COUNT(DISTINCT acc.httpmethod)=4; "												+
			" END; $$ "  																			+
			" LANGUAGE 'plpgsql'; ";
		
		try (Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/LogDB", "postgres", "Fall19");
             Statement statement = conn.createStatement()){
            statement.execute(createFunction);
			conn.close();	
		}
		catch (SQLException e) {
			System.out.println("Problem in creation of the thirteenth Function");
		}
	}

	
	private Integer create_HDFS_FS_Namesystem(Integer ids) {
		String s=" ", s1;
		Integer count=0;
		File file = new File("C:\\Users\\User\\Documents\\workspace-sts-3.9.6.RELEASE\\M149\\files\\HDFS_FS_Namesystem.log"); 
		List<FS_Namesystem> list = new ArrayList<FS_Namesystem>();
	 	try{
	 		String[] parts, parts2, parts3;
	 		Scanner sc = new Scanner(file);
	 		while (sc.hasNextLine()){
	 			s1 = sc.nextLine();
 	 			s=s1;
 		 		FS_Namesystem fsn = new FS_Namesystem();
				ids++;
 				fsn.setId(ids);
	 			parts = s.split(" ", 2);
	 			parts2 = parts[1].split(" ", 2);
	 			s=parts[0]+" "+parts2[0];
	 			SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyy HHmmss");
	            Date parsedDate = dateFormat.parse(s);
	            Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());
	            fsn.setTimeStamp(timestamp);
	            parts=parts2[1].split("ask ", 2);
	            parts=parts[1].split(" to ", 2);
	            parts2=parts[0].split(":", 2);
            	fsn.setSourceIp(parts2[0]);
	            parts=parts[1].split(" ", 2);
            	fsn.setType(parts[0]);
	            parts=parts[1].split(" to datanode\\(s\\) ", 2);

            	parts2=parts[0].split(" ", 2);
	            while (parts2.length>1) {
	            	parts3=parts2[0].split("blk_",2);
	            	if(parts3.length>1) {
	            		fsn.setBlockId(parts3[1]);
	            	}
	            	parts2=parts2[1].split(" ",2);
	            }
            	parts3=parts2[0].split("blk_",2);
            	fsn.setBlockId(parts3[1]);
            	if(parts.length>1) {
                	parts2=parts[1].split(" ", 2);
    	            while (parts2.length>1) {
    		            parts3=parts2[0].split(":", 2);
    	            	fsn.setDestinationIp(parts3[0]);
    	            	parts2=parts2[1].split(" ",2);
    	            }
		            parts3=parts2[0].split(":", 2);
                	fsn.setDestinationIp(parts3[0]);    	            
            	}
            	else {
            		fsn.setDestinationIp(null);
            	}
 				list.add(fsn);
		 		count++;
	 			if (count % 10000 == 0)
	 			{
	 				fS_NamesystemService.insertMultyRow(list);
	 				list = new ArrayList<FS_Namesystem>();
	 			}
	 		}
			fS_NamesystemService.insertMultyRow(list);
	 		sc.close();
 			return ids;
	 	}
	 	catch (Exception e) {
	 		e.printStackTrace();
	 		return -1;
		}
	}
	
	private Integer create_HDFS_DataXceiver(Integer ids)
	{
		String s=" ", s1;
		Integer size, count=0, count2=0;
		File file = new File("C:\\Users\\User\\Documents\\workspace-sts-3.9.6.RELEASE\\M149\\files\\HDFS_DataXceiver.log"); 
		List<DataXceiver> list = new ArrayList<DataXceiver>();
	 	try{
	 		Scanner sc = new Scanner(file);
	 		String[] parts, parts2;
	 		while (sc.hasNextLine()) {
	 			s1 = sc.nextLine();
	 			if(s1.contains("exception") || s1.contains("Exception") || s1.contains("EXCEPTION"))
	 				continue;
 	 			s=s1;
 	 			try {
	 		 		DataXceiver dx = new DataXceiver();
 	 				ids++;
 	 				dx.setId(ids);
		 			parts = s.split(" ", 2);
		 			parts2 = parts[1].split(" ", 2);
		 			s=parts[0]+" "+parts2[0];
		 			SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyy HHmmss");
		            Date parsedDate = dateFormat.parse(s);
		            Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());
		            dx.setTimeStamp(timestamp);
		            
		            parts=parts2[1].split(": ", 2);
		            parts=parts[1].split(" ", 2);
		            if(parts[0].charAt(0)<'0' || parts[0].charAt(0)>'9') {
		            	dx.setType(parts[0]);
		            	parts=parts[1].split("blk_", 2);
		            	parts=parts[1].split(" ", 2);
		            	dx.setBlockId(parts[0]);
		            	parts=parts[1].split("src: /", 2);
		            	parts=parts[1].split(" ", 2);
			            parts2=parts[0].split(":", 2);
		            	dx.setSourceIp(parts2[0]);
		            	parts=parts[1].split("dest: /", 2);
		            	parts=parts[1].split(" ", 2);
    		            parts2=parts[0].split(":", 2);
		            	dx.setDestinationIp(parts2[0]);
		            	if(parts.length>1) {
		            		parts=parts[1].split("of size ", 2);
		            		size=Integer.parseInt(parts[1]);
		            	}
		            	else
		            		size=-1;
		            	dx.setSize(size);
	            }
		            else
		            {
			            parts2=parts[0].split(":", 2);
		            	dx.setSourceIp(parts2[0]);
		            	parts=parts[1].split(" ", 2);
		            	dx.setType(parts[0]);
		            	parts=parts[1].split("blk_", 2);
		            	parts=parts[1].split(" ", 2);
		            	dx.setBlockId(parts[0]);
		            	parts=parts[1].split("to /", 2);
		            	parts=parts[1].split(" ", 2);
    		            parts=parts[0].split(":", 2);
		            	dx.setDestinationIp(parts2[0]);
		            	if(parts.length>1) {
		            		parts=parts[1].split("of size ", 2);
		            		size=Integer.parseInt(parts[1]);
		            	}
		            	else
		            		size=-1;
		            	dx.setSize(size);
		            }
	 				list.add(dx);
		 			count++;
		 			if (count % 10000 == 0)
		 			{
		 				dataXceiverService.insertMultyRow(list);
		 				list = new ArrayList<DataXceiver>();
		 			}
	 			}
	 		 	catch (Exception e) {
	 		 		count2++;
	 		 	}
	 		}
			dataXceiverService.insertMultyRow(list);
			if (count2>0)
				System.out.println("Problem with " + count2 + " DataXceivers");
	 		sc.close();
	 		return ids;
	 	}
	 	catch (Exception e) {
	 		e.printStackTrace();
	 		return -1;
		}
	}
	

	
	private Integer create_accesslog(Integer ids)	
	{
		if (accessService.existTable()==true)
			return 0;
		List<Access> list = new ArrayList<Access>();
 		String s = ""; 		
		int count = 0;
	 	File file = new File("C:\\Users\\User\\Documents\\workspace-sts-3.9.6.RELEASE\\M149\\files\\access.log"); 
	 	try{
	 		Scanner sc = new Scanner(file);
	 		String[] parts, parts2;
	 		while (sc.hasNextLine()) {
	 			s = sc.nextLine();
	 			try {
	 		 		Access acc = new Access();
		 			acc.setId(ids);
		 			acc.setType();
		 			ids++;
	 		 		parts = s.split(" - ", 2);
		            parts2=parts[0].split(":", 2);
		 			acc.setIpAddress(parts2[0]);
		 			parts = parts[1].split(" ", 2);
		 			acc.setUserId(parts[0]);
		 			parts = parts[1].split("] \"", 2);

		 			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MMM/yyyy:HH:mm:ss Z", Locale.US);
		            Date parsedDate = dateFormat.parse(parts[0].substring(1));
		            Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());
		            acc.setTimeStamp(timestamp);

		 			parts = parts[1].split(" ", 2);
		 			acc.setHttpMethod(parts[0]);
		 			String temp = parts[1];
		 			parts = parts[1].split("\" \\d+", 2);
		 			String pt1 = parts[0].trim();
		 		    String pt2 = temp.substring(pt1.length() + 1).trim();
		 			parts2 = parts[0].split(" ", 2);
		 			acc.setResourceRequested(parts2[0]);

		 			parts = pt2.split(" ", 2);

		 			acc.setHttpResponseStatus(Integer.parseInt(parts[0]));
		 			parts = parts[1].split(" \"", 2);
		 			if(parts[0].equals("-"))
		 				acc.setResponseSize(-1);
		 			else	
		 				acc.setResponseSize(Integer.parseInt(parts[0]));
		 			parts = parts[1].split("\" \"", 2);
		 			
		 			if(parts[0].length()==0 || parts[0].equals("-"))
		 				parts[0]=null;
		 			
		 			acc.setReferer(parts[0]);
		 			parts = parts[1].split("\"", 2);
		 			acc.setUserAgentString(parts[0]);	
	 				list.add(acc);
		 			count++;
		 			if (count % 10000 == 0)
		 			{
		 				accessService.insertMultyRow(list);
		 				list = new ArrayList<Access>();
		 			}
	 			}
	 		 	catch (Exception e) {
	 		 		e.printStackTrace();
	 		 	}
	 		}
			accessService.insertMultyRow(list);
			sc.close();
			return ids;
	 	}
	 	catch (Exception e) {
	 		e.printStackTrace();
	 		return 0;
	 	}
	}

	

	
	
	@PostConstruct
	private void init() {
	 	Integer ids=0;
	 	System.out.println("Create Accesslog");
	 	ids = create_accesslog(ids);
	 	if (ids > 0) {
	 		System.out.println("Create DataXceiver");
	 		ids = create_HDFS_DataXceiver(ids);
		 	System.out.println("Create FS_Namesystem");
	 		ids = create_HDFS_FS_Namesystem(ids);
		 	System.out.println("Create Indexes & Functions");
		 	create_indexes();
		 	create_function_1();
		 	create_function_2();
		 	create_function_3();
		 	create_function_4();
		 	create_function_5();
		 	create_function_6();
		 	create_function_7();
		 	create_function_8();
		 	create_function_9();
		 	create_function_10();
		 	create_function_11();
		 	create_function_12();
		 	create_function_13();
		}
	 	else
	 		System.out.println("Database has already created");

	}

 

	public static void main(String[] args) {
		SpringApplication.run(PostgressApplication.class, args);
	}

}

