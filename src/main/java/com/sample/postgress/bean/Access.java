package com.sample.postgress.bean;
import java.sql.Timestamp;

public class Access {

	Integer id;
	String ipAddress;
	String userId;
	Timestamp timeStamp;
	String httpMethod;
	String resourceRequested;
	Integer httpResponseStatus;
	Integer responseSize;
	String referer;
	String userAgentString;
	String type;
	
	@Override
	public boolean equals(Object ob)
	{
		Access acc = (Access) ob;
		return(this.ipAddress==acc.ipAddress && this.userId == acc.userId && this.timeStamp == acc.timeStamp && this.httpMethod==acc.httpMethod && this.resourceRequested==acc.resourceRequested && (int) this.httpResponseStatus == (int) acc.httpResponseStatus && (int) this.responseSize== (int) acc.responseSize && this.referer==acc.referer && this.userAgentString==acc.userAgentString);
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer ids){
		this.id=ids;
	}
	
	public void printAccess(){
		System.out.println(this.ipAddress + " " + this.userId + " " + this.timeStamp + " " + this.httpMethod + " " + this.resourceRequested+ " " + this.httpResponseStatus + " " + this.responseSize + " " + this.referer + " " + this.userAgentString);
	}

	public String getAccessString(){
		return this.id + " " + this.ipAddress + " " + this.userId + " " + this.timeStamp + " " + this.httpMethod + " " + this.resourceRequested+ " " + this.httpResponseStatus + " " + this.responseSize + " " + this.referer + " " + this.userAgentString;
	}

	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}


	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}


	public Timestamp getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(Timestamp timeStamp) {
		this.timeStamp = timeStamp;
	}


	public String getHttpMethod() {
		return httpMethod;
	}
	public void setHttpMethod(String httpMethod) {
		this.httpMethod = httpMethod;
	}


	public String getResourceRequested() {
		return resourceRequested;
	}
	public void setResourceRequested(String resourceRequested) {
		this.resourceRequested = resourceRequested;
	}


	public Integer getHttpResponseStatus() {
		return httpResponseStatus;
	}
	public void setHttpResponseStatus(Integer httpResponseStatus) {
		this.httpResponseStatus = httpResponseStatus;
	}


	public Integer getResponseSize() {
		return responseSize;
	}
	public void setResponseSize(Integer responseSize) {
		this.responseSize = responseSize;
	}


	public String getReferer() {
		return referer;
	}
	public void setReferer(String referer) {
		this.referer = referer;
	}


	public String getUserAgentString() {
		return userAgentString;
	}
	public void setUserAgentString(String userAgentString) {
		this.userAgentString = userAgentString;
	}

	public String getType() {
		return type;
	}
	
	public void setType() {
		this.type = "access";
	}
	
}
