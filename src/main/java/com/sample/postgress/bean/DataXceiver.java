package com.sample.postgress.bean;
import java.sql.Timestamp;

public class DataXceiver {

	Integer id;
	Timestamp timeStamp;
	String blockId;
	String sourceIp;
	String destinationIp;
	Integer size;
	String type;
	
	@Override
	public boolean equals(Object ob)
	{
		DataXceiver dx = (DataXceiver) ob;
		return(this.timeStamp == dx.timeStamp && this.blockId==dx.blockId && this.sourceIp==dx.sourceIp && this.destinationIp == dx.destinationIp && (int) this.size== (int) dx.size && this.type==dx.type);
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer ids){
		this.id=ids;
	}
	
	public void printDataXceiver(){
		System.out.println(this.timeStamp + " " + this.blockId + " " + this.sourceIp + " " + this.destinationIp + " " + this.size + " " + this.type);
	}
	public String getDataXceiverString(){
		return this.id + " " + this.timeStamp + " " + this.blockId + " " + this.sourceIp + " " + this.destinationIp + " " + this.size + " " + this.type;
	}

	public Timestamp getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(Timestamp timeStamp) {
		this.timeStamp = timeStamp;
	}


	public String getBlockId() {
		return blockId;
	}
	public void setBlockId(String blockId) {
		this.blockId = blockId;
	}


	public String getSourceIp() {
		return sourceIp;
	}
	public void setSourceIp(String sourceIp) {
		this.sourceIp = sourceIp;
	}


	public String getDestinationIp() {
		return destinationIp;
	}
	public void setDestinationIp(String destinationIp) {
		this.destinationIp = destinationIp;
	}



	public Integer getSize() {
		return size;
	}
	public void setSize(Integer size) {
		this.size = size;
	}


	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
}
