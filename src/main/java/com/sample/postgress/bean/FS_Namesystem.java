package com.sample.postgress.bean;
import java.sql.Timestamp;
import java.util.List;
import java.util.ArrayList;


public class FS_Namesystem {

	Integer id;
	Timestamp timeStamp;
	List<String> blockId;
	String sourceIp;
	List<String> destinationIp;
	String type;

	public FS_Namesystem() {
		blockId = new ArrayList<String>();
		destinationIp = new ArrayList<String>();
	}
	
	public String getFS_NamesystemString(){
		String s = this.id + " " + this.timeStamp + " block(s):";
		for (String block : this.blockId){
			s+=" " + block;
		}
		s+=" " + sourceIp;
		if(this.destinationIp.size()>0)
				s += " Destination(s):";
		for (String destination: this.destinationIp){
			s+=" " + destination;
		}
		s+=" " + type;
		return s;
	}

	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer ids){
		this.id=ids;
	}
	
	public Timestamp getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(Timestamp timeStamp) {
		this.timeStamp = timeStamp;
	}


	public List<String> getBlockId() {
		return blockId;
	}
	public void setBlockId(String blockId) {
		this.blockId.add(blockId);
	}


	public String getSourceIp() {
		return sourceIp;
	}
	public void setSourceIp(String sourceIp) {
		this.sourceIp = sourceIp;
	}


	public List<String> getDestinationIp() {
		return destinationIp;
	}
	public void setDestinationIp(String destinationIp) {
		this.destinationIp.add(destinationIp);
	}

	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
}
