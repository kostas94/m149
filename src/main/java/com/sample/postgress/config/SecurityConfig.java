package com.sample.postgress.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
 
@Configuration
@EnableAutoConfiguration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
 
	@Autowired
	DataSource dataSource;
 
	@Autowired
	public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {
		auth.jdbcAuthentication().dataSource(dataSource)
				.usersByUsernameQuery("select username,password, enabled from users where username=?")
				.authoritiesByUsernameQuery("select username, role from user_roles where username=?");
	}
 
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().antMatchers("/", "/home").permitAll().antMatchers("/registration").permitAll()
		.antMatchers("/question1").hasRole("USER").antMatchers("/question1Results").hasRole("USER").antMatchers("/question2").hasRole("USER")
		.antMatchers("/question2Results").hasRole("USER").antMatchers("/question3").hasRole("USER").antMatchers("/question3Results").hasRole("USER")
		.antMatchers("/history").hasRole("USER").antMatchers("/insertLog").hasRole("USER").antMatchers("/alreadyLoggedIn").hasRole("USER")
		.antMatchers("/insertAccessLog").hasRole("USER").antMatchers("/insertFS_NameLog").hasRole("USER").antMatchers("/insertDataXceiverLog").hasRole("USER")
		.anyRequest().authenticated().and().formLogin().loginPage("/login").permitAll().and().logout()
		.permitAll();
		http.exceptionHandling().accessDeniedPage("/403");
	}
}