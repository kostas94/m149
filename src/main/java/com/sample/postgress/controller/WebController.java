package com.sample.postgress.controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.ui.Model;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.sample.postgress.service.AccessService;
import com.sample.postgress.service.DataXceiverService;
import com.sample.postgress.service.FS_NamesystemService;
import com.sample.postgress.bean.*;

import org.springframework.jdbc.core.RowMapper;
import java.util.List;
import java.util.Locale;
import java.util.ArrayList;
import java.util.Date;

import javax.annotation.Resource;
import org.springframework.web.bind.annotation.RequestParam;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;


@Controller
@RequestMapping(value="/", produces="application/json")
public class WebController {
	
	@Resource 
	AccessService accessService;
	@Resource 
	DataXceiverService dataXceiverService;
	@Resource 
	FS_NamesystemService fS_NamesystemService;

	
	public WebController(NamedParameterJdbcTemplate template) {  
        this.template = template;  
	}
	NamedParameterJdbcTemplate template; 
	
	
	
	
	@RequestMapping(value="/")
    public String home(Authentication authentication){
		if(authentication!=null)
			return "home2";
		return "home";
    }

	
	
	
	@GetMapping(value="/registration")
    public String registration(User user, Authentication authentication){
		if(authentication!=null)
			return "alreadyLoggedIn";
        return "registration";
    }

	
	@PostMapping(value="/registration")
    public String registration(Authentication authentication, User user, Model model){
		if(user.getUserName().equals("") || user.getFirstName().equals("") || user.getLastName().equals("") || user.getUserEmail().equals("") || user.getUserPassword().equals("")) {
			model.addAttribute("return_page", "registration");
			return "EmptyFields";
		}
		else
		{
			String query = "select userName from users where userName='"+user.getUserName()+"'";
			List<String> data = template.query(query, new RowMapper<String>(){
				public String mapRow(ResultSet rs, int rowNum) 
									throws SQLException {
                        return rs.getString("userName");
                }
			});
			if (data.size()>0)
				return "AlreadyUser";
		}
		String sql = "INSERT INTO users(userName, password, firstName, lastName, userEmail, userAddress, enabled)" + "values(:userName, :password, :firstName, :lastName, :userEmail, :userAddress, :enabled);";
	    KeyHolder holder = new GeneratedKeyHolder();
	    SqlParameterSource param = new MapSqlParameterSource()
	    	.addValue("userName", user.getUserName())
	    	.addValue("password", user.getUserPassword())
			.addValue("firstName", user.getFirstName())
			.addValue("lastName", user.getLastName())
			.addValue("userEmail", user.getUserEmail())
			.addValue("userAddress", user.getUserAddress())
			.addValue("enabled", true);
	    template.update(sql,param, holder);

		sql = "INSERT INTO user_roles(userName, role)" + "values(:userName, :role);";
		holder = new GeneratedKeyHolder();
		param = new MapSqlParameterSource()
			.addValue("userName", user.getUserName())
			.addValue("role", "ROLE_USER");
		template.update(sql,param, holder);

		sql = "INSERT INTO history(userName, timeStamp, action_description)" + "values(:userName, :timeStamp, :action_description);";
		holder = new GeneratedKeyHolder();
		param = new MapSqlParameterSource()
			.addValue("userName", user.getUserName())
			.addValue("timeStamp", new Timestamp(System.currentTimeMillis()))
			.addValue("action_description", "The user "+  user.getUserName() + " signed up to the database.");
		template.update(sql,param, holder);
		return "Success_Registration";
    }

	
	
	@GetMapping(value="/question1")
    public String question1(Authentication authentication, Model model){
		model.addAttribute("from_date", new String());
		model.addAttribute("until_date", new String());
        return "question1";
    }
	
	@PostMapping(value="/question1")
    public String question1(Authentication authentication, Model model, @RequestParam String from_date, @RequestParam String until_date) throws SQLException{
		Statement stmt = null;
		if(from_date.equals("") || until_date.equals("")) {
			model.addAttribute("return_page", "question1");
			return "EmptyFields";
		}			
		String query = "select type, total_logs from question1('" + from_date + "', '" + until_date + "')";
		List<String> type_list = new ArrayList<String>();
	    List<Integer> total_logs_list = new ArrayList<Integer>();
	    try {
			Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/LogDB", "postgres", "Fall19");
			stmt = con.createStatement();
	        ResultSet rs = stmt.executeQuery(query);
	        while (rs.next()) {
	            String type = rs.getString("type");
	            Integer total_log = rs.getInt("total_logs");
	            type_list.add(type);
	            total_logs_list.add(total_log);
	        }
	        model.addAttribute("types", type_list);
	        model.addAttribute("total_logs", total_logs_list);
	        model.addAttribute("from_day", from_date);
	        model.addAttribute("until_day", until_date);
	        if (stmt != null)
	        	stmt.close();

	        String sql = "INSERT INTO history(userName, timeStamp, action_description)" + "values(:userName, :timeStamp, :action_description);";
					 KeyHolder holder = new GeneratedKeyHolder();
					 SqlParameterSource param = new MapSqlParameterSource()
				        .addValue("userName", authentication.getName())
				        .addValue("timeStamp", new Timestamp(System.currentTimeMillis()))
				        .addValue("action_description", "The user "+  authentication.getName() + " search for question 1 with time range: " + from_date + "-" + until_date + ".");
				        template.update(sql,param, holder);

	    } catch (SQLException e) {
	    	e.printStackTrace();
	        if (stmt != null)
	        	stmt.close();
	    }     	    
        return "question1Results";
    }


	@GetMapping(value="/question2")
    public String question2(Authentication authentication, Model model){
		model.addAttribute("from_date", new String());
		model.addAttribute("until_date", new String());
		model.addAttribute("action_type", new String());
        return "question2";
    }

	@PostMapping(value="/question2")
    public String question2(Authentication authentication, Model model, @RequestParam String from_date, @RequestParam String until_date, @RequestParam String action_type) throws SQLException{
		if(from_date.equals("") || until_date.equals("") || action_type.equals("")) {
			model.addAttribute("return_page", "question2");
			return "EmptyFields";
		}			
		Statement stmt = null;
		String query = "select day_of_time_range, total_logs from question2('" + from_date + "', '" + until_date + "', '" + action_type + "')";
		List<String> day_of_time_range_list = new ArrayList<String>();
	    List<Integer> total_logs_list = new ArrayList<Integer>();
	    try {
			Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/LogDB", "postgres", "Fall19");
			stmt = con.createStatement();
	        ResultSet rs = stmt.executeQuery(query);
	        while (rs.next()) {
	            String day_of_time_range = rs.getString("day_of_time_range");
	            Integer total_log = rs.getInt("total_logs");
	            day_of_time_range_list.add(day_of_time_range);
	            total_logs_list.add(total_log);
	        }
	        model.addAttribute("days", day_of_time_range_list);
	        model.addAttribute("total_logs", total_logs_list);
	        model.addAttribute("from_day", from_date);
	        model.addAttribute("until_day", until_date);
	        model.addAttribute("action", action_type);
	        if (stmt != null)
	        	stmt.close();

	        String sql = "INSERT INTO history(userName, timeStamp, action_description)" + "values(:userName, :timeStamp, :action_description);";
					 KeyHolder holder = new GeneratedKeyHolder();
					 SqlParameterSource param = new MapSqlParameterSource()
				        .addValue("userName", authentication.getName())
				        .addValue("timeStamp", new Timestamp(System.currentTimeMillis()))
				        .addValue("action_description", "The user "+  authentication.getName() + " search for question 2 with time range: " + from_date + "-" + until_date + " and action: " + action_type + ".");
				        template.update(sql,param, holder);

	    } catch (SQLException e) {
	    	e.printStackTrace();
	        if (stmt != null)
	        	stmt.close();
	    }     	    
        return "question2Results";
    }

	
	
	@GetMapping(value="/question3")
    public String question3(Authentication authentication, Model model){
		model.addAttribute("timestamp", new String());

		return "question3";
    }	
	
	@PostMapping(value="/question3")
    public String question3(Authentication authentication, Model model, @RequestParam String timestamp) throws SQLException{
		if(timestamp.equals("")) {
			model.addAttribute("return_page", "question3");
			return "EmptyFields";
		}			

		Statement stmt = null;
		String query = "select ipAddress, most_common_log_type from question3('"+ timestamp +"')";
    	List<String> ipaddresslist = new ArrayList<String>();
    	List<String> most_common_log_list = new ArrayList<String>();
    	try {
    		Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/LogDB", "postgres", "Fall19");
    		stmt = con.createStatement();
    	    ResultSet rs = stmt.executeQuery(query);
    	    while (rs.next()) {
    	    	String ipaddress = rs.getString("ipAddress");
    	        String most_common_log = rs.getString("most_common_log_type");
    	        ipaddresslist.add(ipaddress);
    	        most_common_log_list.add(most_common_log);
    	    }
    	    model.addAttribute("ipaddresses", ipaddresslist);
    	    model.addAttribute("most_common_logs", most_common_log_list);
    	    model.addAttribute("timestamp", timestamp);
    	    if (stmt != null)
    	    	stmt.close();

    	    String sql = "INSERT INTO history(userName, timeStamp, action_description)" + "values(:userName, :timeStamp, :action_description);";
    		KeyHolder holder = new GeneratedKeyHolder();
    		SqlParameterSource param = new MapSqlParameterSource()
    				.addValue("userName", authentication.getName())
    				.addValue("timeStamp", new Timestamp(System.currentTimeMillis()))
    				.addValue("action_description", "The user "+  authentication.getName() + " search for question 3 with timestamp: " + timestamp + ".");
    		template.update(sql,param, holder);

    	} catch (SQLException e) {
    		e.printStackTrace();
    	    if (stmt != null)
    	    	stmt.close();
    	}     	    
		return "question3Results";
    }


	
	
	
	@GetMapping(value="/search")
    public String search(Authentication authentication, Model model){
		model.addAttribute("ip_address", new String());
		model.addAttribute("type", new String());

		return "searchByIp";
    }

	
	
	@PostMapping(value="/search")
    public String search(Authentication authentication, Model model, @RequestParam String ip_address,  @RequestParam("radioName") String type) throws SQLException{
		if(ip_address.equals("")) {
			model.addAttribute("return_page", "search");
			return "EmptyFields";
		}			
		try {
   	    	Integer count = 1;
			String job="";
				Statement stmt = null;
			if (type.equals("source_ip"))
			{
				String query = " select timestamp, type, block_id, destination_ip, size " 	+
				" from " 																				+
				"	(select cfbl.id, cfbl.timestamp, cfbl.type, cfbl.block_id, d.destination_ip "		+
				"	from "																				+
				"		(select cf.id, cf.timestamp, cf.type, bl.block_id "								+
				"		from "																			+
				"			common_fields as cf inner join blocks as bl on cf.id = bl.id " 				+
				"		where cf.ipaddress='" + ip_address + "') as cfbl "								+
				"		left join destinations as d on d.id = cfbl.id) as cfbld "						+
				"		left join sizes as s on cfbld.id = s.id; ";
			    List<String> results = new ArrayList<String>();

    			Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/LogDB", "postgres", "Fall19");
    			stmt = con.createStatement();
    	        ResultSet rs = stmt.executeQuery(query);
    	        while (rs.next()) {

    	        	String s = count + ". Timestamp: " + rs.getString("timestamp") + " Type: " + rs.getString("type") + " Block: " + rs.getString("block_id");
    	        	String size = rs.getString("size");
    	        	if(!rs.wasNull() && !size.equals("-1"))
    	        		s += " Size: " + size;
    	        	String destination = rs.getString("destination_ip");
    	        	if(!rs.wasNull())
    	        		s += " Destination: " + destination;
    	        	s+=".";
    	        	results.add(s);    	        
    	        	count++;
    	        }
    	        query = " select timestamp, type, httpmethod, httpresponsestatus, referer, resourcerequested, responsesize, useragentstring, userid " 	+
    	        		" from common_fields natural join accesslog "																					+
    	        		" where ipaddress='" + ip_address + "';";
    	        
    			stmt = con.createStatement();
    	        rs = stmt.executeQuery(query);
    	        while (rs.next()) {

    	        	String userid= rs.getString("userid");
    	        	String s = count + ".";
    	        	if(!userid.equals("-"))
    	        		s += " User Id: " + userid;

    	        	s += " Timestamp: " + rs.getString("timestamp") + " Type: " + rs.getString("type") + " HTTP Mmethod: " + rs.getString("httpmethod") + 
    	        		 " HTTP Response Status: " + rs.getString("httpresponsestatus");
    	        	String referer = rs.getString("referer");
    	        	if(!rs.wasNull())
    	        		s += " Referer: " + referer;
    	        	s += " Resourse Requested: " + rs.getString("resourcerequested");
    	        	String size = rs.getString("responsesize");
    	        	if(!rs.wasNull() && !size.equals("-1"))
    	        		s += " Size: " + size;
    	        	s+=" User Agent String: " + rs.getString("useragentstring") + ".";
    	        	results.add(s);    	        
    	        	count++;
    	        }
    	        
    	        model.addAttribute("type", "Source Ip");
    	        model.addAttribute("ip_address", ip_address);
    	        model.addAttribute("results", results);
    	        if (stmt != null)
    	        	stmt.close();

    	        job = "The user "+  authentication.getName() + " search for logs with Source Ip: " + ip_address + ".";
			}
			else
			{
					String query = 	"select ipaddress, timestamp, type, block_id, size " +
									" from destinations as d left join sizes as s on d.id=s.id " +
									" inner join common_fields as cf on d.id=cf.id " +
									" inner join blocks as bl on bl.id=cf.id " +
									" where destination_ip='" + ip_address + "';";
		    	    List<String> results = new ArrayList<String>();
		    			Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/LogDB", "postgres", "Fall19");
		    			stmt = con.createStatement();
		    	        ResultSet rs = stmt.executeQuery(query);
		    	        while (rs.next()) {
	
		    	        	String s = count + ". Ip Address: " + rs.getString("ipaddress") + " Timestamp: " + rs.getString("timestamp") + " Type: " + rs.getString("type") + " Block: " + rs.getString("block_id");
		    	        	String size = rs.getString("size");
		    	        	if(size!=null && !size.equals("-1"))
		    	        		s += " Size: " + size;
		    	        	s+=".";
		    	        	results.add(s);
		    	        	count++;
		    	        }
		    	        model.addAttribute("type", "Destination Ip");
		    	        model.addAttribute("ip_address", ip_address);
		    	        model.addAttribute("results", results);
		    	        if (stmt != null)
		    	        	stmt.close();
		    	        
		    	        job = "The user "+  authentication.getName() + " search for logs with Destination Ip: " + ip_address + ".";
			}
	        String sql = "INSERT INTO history(userName, timeStamp, action_description)" + "values(:userName, :timeStamp, :action_description);";
					 KeyHolder holder = new GeneratedKeyHolder();
					 SqlParameterSource param = new MapSqlParameterSource()
				        .addValue("userName", authentication.getName())
				        .addValue("timeStamp", new Timestamp(System.currentTimeMillis()))
				        .addValue("action_description", job );
				        template.update(sql,param, holder);
			return "searchByIpResults";
   	    }
		catch (Exception e) {
			return "problemWithSearch";				
		}
    }
	
	
	
	@RequestMapping(value="/insertLog")
    public String instertLog(Authentication authentication){
        return "insertLog";
	}

	
	@GetMapping(value="/insertAccessLog")
    public String instertAccessLog(Authentication authentication, Model model){
			model.addAttribute("ip_address", new String());
			model.addAttribute("user_id", new String());
			model.addAttribute("timestamp", new String());
			model.addAttribute("http_method", new String());
			model.addAttribute("resource_requested", new String());
			model.addAttribute("http_response_status", new String());
			model.addAttribute("response_size", new String());
			model.addAttribute("referer", new String());
			model.addAttribute("user_agent", new String());

        return "insertAccessLog";
	}

	
	@PostMapping(value="/insertAccessLog")
    public String instertAccessLog(Authentication authentication, Model model, @RequestParam String ip_address,  @RequestParam String user_id, @RequestParam String timestamp, @RequestParam String http_method, @RequestParam String resource_requested, @RequestParam String http_response_status, @RequestParam String response_size, @RequestParam String referer, @RequestParam String user_agent) throws SQLException{
		if(ip_address.equals("") || http_method.equals("") || resource_requested.equals("") || http_response_status.equals("") || referer.equals("") || user_agent.equals("")) {
			model.addAttribute("return_page", "insertAccessLog");
			return "EmptyFields";
		}			
		Access acc = new Access();
		if(user_id.equals(""))
			user_id="-";
		if(response_size.equals(""))
			response_size="-1";
		
		Statement stmt = null;
		String query = "select max(id) as new_id from common_fields";
	    try {
			Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/LogDB", "postgres", "Fall19");
			stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			rs.next();
	        Integer new_id = rs.getInt("new_id")+1;

	        acc.setId(new_id);
 			acc.setType();
 			acc.setIpAddress(ip_address);
 			acc.setUserId(user_id);
 			if(timestamp.equals(""))
 				acc.setTimeStamp(new Timestamp(System.currentTimeMillis()));
 			else
 			{
	 			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
	            Date parsedDate = dateFormat.parse(timestamp);
	            Timestamp tmstamp = new java.sql.Timestamp(parsedDate.getTime());
				acc.setTimeStamp(tmstamp);
 			}
 			acc.setHttpMethod(http_method);
 			acc.setResourceRequested(resource_requested);
 			acc.setHttpResponseStatus(Integer.parseInt(http_response_status));
			acc.setResponseSize(Integer.parseInt(response_size));
 			acc.setReferer(referer);
 			acc.setUserAgentString(user_agent);	
 			
 			accessService.insertAccess(acc); 			

 			String sql = "INSERT INTO history(userName, timeStamp, action_description)" + "values(:userName, :timeStamp, :action_description);";
 			KeyHolder holder = new GeneratedKeyHolder();
 			SqlParameterSource param = new MapSqlParameterSource()
				.addValue("userName", authentication.getName())
				.addValue("timeStamp", new Timestamp(System.currentTimeMillis()))
				.addValue("action_description", "The user "+  authentication.getName() + " inserted into the database the AccessLog: " + acc.getAccessString() + ".");
			template.update(sql,param, holder);
 			
			return "successInsertion";	
	    } catch (Exception e) {
	    	e.printStackTrace();
	        if (stmt != null)
	        	stmt.close();
			return "problemWithInsertion";	
		}     	    
	}
	
	
	@GetMapping(value="/insertDataXceiverLog")
    public String instertDataXceiverLog(Authentication authentication, Model model){
		model.addAttribute("timestamp", new String());
		model.addAttribute("block_id", new String());
		model.addAttribute("source_ip", new String());
		model.addAttribute("destination_ip", new String());
		model.addAttribute("o_size", new String());
		model.addAttribute("type", new String());

        return "insertDataXceiverLog";
 
	}

	@PostMapping(value="/insertDataXceiverLog")
    public String instertDataXceiverLog(Authentication authentication, Model model, @RequestParam String timestamp, @RequestParam String block_id, @RequestParam String source_ip, @RequestParam String destination_ip, @RequestParam String o_size, @RequestParam String type) throws SQLException{
		if(block_id.equals("") || source_ip.equals("") || destination_ip.equals("") || type.equals("")) {
			model.addAttribute("return_page", "insertDataXceiverLog");
			return "EmptyFields";
		}			
		DataXceiver dx = new DataXceiver();
		if(o_size.equals(""))
			o_size="-1";
		
		Statement stmt = null;
		String query = "select max(id) as new_id from common_fields";
	    try {
			Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/LogDB", "postgres", "Fall19");
			stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			rs.next();
	        Integer new_id = rs.getInt("new_id")+1;

	        dx.setId(new_id);
 			dx.setType(type);
 			dx.setSourceIp(source_ip);
 			dx.setDestinationIp(destination_ip);
 			if(timestamp.equals(""))
 				dx.setTimeStamp(new Timestamp(System.currentTimeMillis()));
 			else
 			{
	 			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
	            Date parsedDate = dateFormat.parse(timestamp);
	            Timestamp tmstamp = new java.sql.Timestamp(parsedDate.getTime());
				dx.setTimeStamp(tmstamp);
 			}
 			dx.setBlockId(block_id);
			dx.setSize(Integer.parseInt(o_size));
 			
 			dataXceiverService.insertDataXceiver(dx); 			

 			String sql = "INSERT INTO history(userName, timeStamp, action_description)" + "values(:userName, :timeStamp, :action_description);";
					 KeyHolder holder = new GeneratedKeyHolder();
					 SqlParameterSource param = new MapSqlParameterSource()
				        .addValue("userName", authentication.getName())
				        .addValue("timeStamp", new Timestamp(System.currentTimeMillis()))
				        .addValue("action_description", "The user "+  authentication.getName() + " inserted into the database the DataXceiver: " + dx.getDataXceiverString() + ".");
				        template.update(sql,param, holder);
 			
			return "successInsertion";	
	    } catch (Exception e) {
	    	e.printStackTrace();
	        if (stmt != null)
	        	stmt.close();
			return "problemWithInsertion";	
		}     	    


	}
	
	
	@GetMapping(value="/insertFS_NameLog")
    public String instertFS_NameLog(Authentication authentication, Model model){
		model.addAttribute("timestamp", new String());
		model.addAttribute("block_id", new String());
		model.addAttribute("source_ip", new String());
		model.addAttribute("destination_ip", new String());
		model.addAttribute("type", new String());

        return "insertFS_NameLog";
 	}

	@PostMapping(value="/insertFS_NameLog")
    public String instertFS_NameLog(Authentication authentication, Model model, @RequestParam String timestamp, @RequestParam String block_id, @RequestParam String source_ip, @RequestParam String destination_ip, @RequestParam String type) throws SQLException{
		if(source_ip.equals("") || type.equals("")) {
			model.addAttribute("return_page", "insertFS_NameLog");
			return "EmptyFields";
		}			
		FS_Namesystem fsn = new FS_Namesystem();
		
		Statement stmt = null;
		String query = "select max(id) as new_id from common_fields";
	    try {
			Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/LogDB", "postgres", "Fall19");
			stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			rs.next();
	        Integer new_id = rs.getInt("new_id")+1;

	        fsn.setId(new_id);
	        fsn.setType(type);
	        fsn.setSourceIp(source_ip);
 			if(timestamp.equals(""))
 				fsn.setTimeStamp(new Timestamp(System.currentTimeMillis()));
 			else
 			{
	 			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
	            Date parsedDate = dateFormat.parse(timestamp);
	            Timestamp tmstamp = new java.sql.Timestamp(parsedDate.getTime());
	            fsn.setTimeStamp(tmstamp);
 			}

 			String[] s;
 			if(destination_ip!="") {
 				s=destination_ip.split(",", 2);
	 			while (s.length>1) {
	 				if(s[0].charAt(0)==' ')
	 		 			fsn.setDestinationIp(s[0].substring(1));
	 				else
	 		 			fsn.setDestinationIp(s[0]);
	 	 			s=s[1].split(",", 2);
	 			}
				if(s[0].charAt(0)==' ')
 		 			fsn.setDestinationIp(s[0].substring(1));
 				else
 		 			fsn.setDestinationIp(s[0]);
 			}

 			if(block_id!="") {
 				s=block_id.split(",", 2);
	 			while (s.length>1) {
	 				if(s[0].charAt(0)==' ')
	 					fsn.setBlockId(s[0].substring(1));
	 				else
	 					fsn.setBlockId(s[0]);
	 	 			s=s[1].split(",", 2);
	 			}
				if(s[0].charAt(0)==' ')
					fsn.setBlockId(s[0].substring(1));
 				else
 					fsn.setBlockId(s[0]);
 			}			
 			System.out.println(fsn.getFS_NamesystemString());
 			
 			fS_NamesystemService.insertFS_Namesystem(fsn); 			

 			String sql = "INSERT INTO history(userName, timeStamp, action_description)" + "values(:userName, :timeStamp, :action_description);";
					 KeyHolder holder = new GeneratedKeyHolder();
					 SqlParameterSource param = new MapSqlParameterSource()
				        .addValue("userName", authentication.getName())
				        .addValue("timeStamp", new Timestamp(System.currentTimeMillis()))
				        .addValue("action_description", "The user "+  authentication.getName() + " inserted into the database the FS_Namesystem: " + fsn.getFS_NamesystemString() + ".");
				        template.update(sql,param, holder);
			
			return "successInsertion";	
	    } catch (Exception e) {
	    	e.printStackTrace();
	        if (stmt != null)
	        	stmt.close();
			return "problemWithInsertion";	
		}     	    


	}
	
	
    @RequestMapping(value="/history")
    public String history(Authentication authentication, Model model) throws SQLException{
    	   	Statement stmt = null;
    	    String query =
    	        "select action_description, timestamp " +
    	        "from history where username = '" + authentication.getName() + "'";
    	    List<String> actionlist = new ArrayList<String>();
    	    List<Timestamp> timestamplist = new ArrayList<Timestamp>();
    	    try {
    			Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/LogDB", "postgres", "Fall19");
    			stmt = con.createStatement();
    	        ResultSet rs = stmt.executeQuery(query);
    	        while (rs.next()) {
    	            String action = rs.getString("action_description");
    	            Timestamp timestamp = rs.getTimestamp("timestamp");
    	            actionlist.add(action);
    	            timestamplist.add(timestamp);
    	        }
    	        model.addAttribute("actionlist", actionlist);
    	        model.addAttribute("timestamplist", timestamplist);
    	        if (stmt != null)
    	        	stmt.close();

    	        String sql = "INSERT INTO history(userName, timeStamp, action_description)" + "values(:userName, :timeStamp, :action_description);";
    					 KeyHolder holder = new GeneratedKeyHolder();
    					 SqlParameterSource param = new MapSqlParameterSource()
    				        .addValue("userName", authentication.getName())
    				        .addValue("timeStamp", new Timestamp(System.currentTimeMillis()))
    				        .addValue("action_description", "The user "+  authentication.getName() + " saw the history.");
    				        template.update(sql,param, holder);

    	    } catch (SQLException e) {
    	    	e.printStackTrace();
    	        if (stmt != null)
    	        	stmt.close();
    	    }     	    
    	return "history";
    }
	
    
    @RequestMapping(value="/login")
    public String login(Authentication authentication){
		if(authentication!=null)
			return "alreadyLoggedIn";
        return "login";
    }
   
    @RequestMapping(value="/403")
    public String Error403(Authentication authentication){
        return "403";
    }
}
