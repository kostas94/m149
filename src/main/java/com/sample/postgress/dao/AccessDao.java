package com.sample.postgress.dao;

import java.util.List;

import com.sample.postgress.bean.Access;

public interface AccessDao {

	List<Access> findAll();

	boolean existTable();
	
	void insertMultyCommonFields(List<Access> accs);
	void insertMultyAccessLog(List<Access> accs);

	void insertCommonFields(Access accs);
	void insertAccessLog(Access accs);
	void insertAccess(Access acc);

	void insertMultyRow(List<Access> accs);

	List<Access> findOne(Access acc);
	
	public void deleteAccess(Access acc);
}
