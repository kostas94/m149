package com.sample.postgress.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.sample.postgress.bean.Access;
import com.sample.postgress.mapper.AccessRowMapper;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.BatchUpdateException;
import java.sql.Statement;

@Repository

public class AccessDaoImpl implements AccessDao{

	
	public AccessDaoImpl(NamedParameterJdbcTemplate template) {  
        this.template = template;  
	}


	NamedParameterJdbcTemplate template;  


	
	@Override
	public List<Access> findAll() {
		return template.query("select * from accesslog natural join common_fields", new AccessRowMapper());
	}

	
	
	@Override
	public boolean existTable(){
		if(template.query("select * from common_fields natural join accesslog where id=0", new AccessRowMapper()).size()>0)
			return true;
		return false;
	}

	
	@Override	
	public void insertMultyCommonFields(List<Access> accs) {
		String sql = " INSERT INTO common_fields(id, ipAddress, timeStamp, type)" + "values(?,?,?,?);";
		int count=0;
		try{
			Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/LogDB", "postgres", "Fall19");

			PreparedStatement ps = connection.prepareStatement(sql);
			for (Access acc : accs) {
		        ps.setInt(1, acc.getId());
				ps.setString(2, acc.getIpAddress());
		        ps.setTimestamp(3, acc.getTimeStamp());
		        ps.setString(4, acc.getType());
		        ps.addBatch();
			}
			ps.executeBatch();
			ps.close();
			connection.close();	
		}
		catch (BatchUpdateException ex) {
			int[] updateCount = ex.getUpdateCounts();
         
	        count = 0;
	        for (int i : updateCount) {
	            if  (i == Statement.EXECUTE_FAILED) {
						try{
 							insertCommonFields(accs.get(count));
 						}
 						catch (Exception e) {
 						}						
	            }
	            count++;
	             
	        }
	    }
		catch (SQLException e) {
	        e.printStackTrace();
		}
	}



	@Override
	public void insertMultyAccessLog(List<Access> accs) {
		String sql = " INSERT INTO accesslog(id, userId, httpMethod, resourceRequested, httpResponseStatus, responseSize, referer, userAgentString)" + "values(?,?,?,?,?,?,?,?);";
		int count=0;
		try{
			Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/LogDB", "postgres", "Fall19");

			PreparedStatement ps = connection.prepareStatement(sql);
			for (Access acc : accs) {
		        ps.setInt(1, acc.getId());
		        ps.setString(2, acc.getUserId());
		        ps.setString(3, acc.getHttpMethod());
		        ps.setString(4, acc.getResourceRequested());
		        ps.setInt(5, acc.getHttpResponseStatus());
		        ps.setInt(6, acc.getResponseSize());
		        ps.setString(7, acc.getReferer());
		        ps.setString(8, acc.getUserAgentString());
		        ps.addBatch();
			}
			ps.executeBatch();
			ps.close();
			connection.close();	
		}
		catch (BatchUpdateException ex) {
			int[] updateCount = ex.getUpdateCounts();
         
	        count = 0;
	        for (int i : updateCount) {
	            if  (i == Statement.EXECUTE_FAILED) {
						try{
 							insertAccessLog(accs.get(count));
 						}
 						catch (Exception e) {
 							System.out.println("Problem with access: ");
 							accs.get(count).printAccess();
 							deleteAccess(accs.get(count));
 						}						
	            }
	            count++;
	             
	        }
	    }
		catch (SQLException e) {
	        e.printStackTrace();
		}

	}
	
	@Override
	public void insertMultyRow(List<Access> accs) {
		insertMultyCommonFields(accs);
		insertMultyAccessLog(accs);
	}


	@Override
	public List<Access> findOne(Access acc) {
		return template.query("Select * from accesslog where ipAddress='" + acc.getIpAddress() + "'" , new AccessRowMapper());
	}
	
	
	
	
	@Override
	public void insertCommonFields(Access acc) {
		 final String sql = "INSERT INTO common_fields(id, ipAddress, timeStamp, type)" + "values(:id, :ipAddress, :timeStamp, :type);";
	        KeyHolder holder = new GeneratedKeyHolder();
	        SqlParameterSource param = new MapSqlParameterSource()
   	        .addValue("id", acc.getId())
   	        .addValue("ipAddress", acc.getIpAddress())
			.addValue("timeStamp", acc.getTimeStamp())
			.addValue("type", acc.getType());
	        template.update(sql,param, holder);
	}

	@Override
	public void insertAccessLog(Access acc) {
		 final String sql = " INSERT INTO accesslog(id, userId, httpMethod, resourceRequested, httpResponseStatus, responseSize, referer, userAgentString)" + "values(:id, :userId, :httpMethod, :resourceRequested, :httpResponseStatus, :responseSize, :referer, :userAgentString);";

	        KeyHolder holder = new GeneratedKeyHolder();
	        SqlParameterSource param = new MapSqlParameterSource()
   	        .addValue("id", acc.getId())
			.addValue("userId", acc.getUserId())
			.addValue("httpMethod", acc.getHttpMethod())
			.addValue("resourceRequested", acc.getResourceRequested())
			.addValue("httpResponseStatus", acc.getHttpResponseStatus())
			.addValue("responseSize", acc.getResponseSize())
			.addValue("referer", acc.getReferer())
			.addValue("userAgentString", acc.getUserAgentString());
	        template.update(sql,param, holder);
	}

	@Override	
	public void insertAccess(Access acc) {
		try{
			insertCommonFields(acc);
			insertAccessLog(acc);
		}
		catch (Exception e) {
			System.out.println("Problem with access: ");
			acc.printAccess();
			deleteAccess(acc);
		}						
		
	}

	
	@Override
	public void deleteAccess(Access acc) {
		try {
			final String sql = "delete from accesslog where id=:id";
				 
	
			 Map<String,Object> map=new HashMap<String,Object>();  
			 map.put("id", acc.getId());
		
			 template.execute(sql,map,new PreparedStatementCallback<Object>() {  
			    @Override  
			    public Object doInPreparedStatement(PreparedStatement ps)  
			            throws SQLException, DataAccessException {  
			        return ps.executeUpdate();  
			    }  
			});  
		}
		finally {
			try {
				final String sql = "delete from common_fields where id=:id";
					 
		
				 Map<String,Object> map=new HashMap<String,Object>();  
				 map.put("id", acc.getId());
			
				 template.execute(sql,map,new PreparedStatementCallback<Object>() {  
					    @Override  
					    public Object doInPreparedStatement(PreparedStatement ps)  
					            throws SQLException, DataAccessException {  
					        return ps.executeUpdate();  
					    }  
					});  
			}
			finally {
				
			}
		}
		
	 
	}
	
}
