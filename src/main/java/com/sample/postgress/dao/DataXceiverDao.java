package com.sample.postgress.dao;

import java.util.List;

import com.sample.postgress.bean.DataXceiver;

public interface DataXceiverDao {

	List<DataXceiver> findAll();

	void insertMultyCommonFields(List<DataXceiver> dxs);
	void insertMultyBlocks(List<DataXceiver> dxs);
	void insertMultyDestinations(List<DataXceiver> dxs);
	void insertMultySizes(List<DataXceiver> dxs);
	
	void insertCommonFields(DataXceiver dx);
	void insertBlockId(DataXceiver dx);
	void insertDestinationIp(DataXceiver dx);
	void insertSize(DataXceiver dx);
	void insertDataXceiver(DataXceiver dx);
	void insertMultyRow(List<DataXceiver> dxs);
	
	public void deleteDataXceiver(DataXceiver dx);
}
