package com.sample.postgress.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.sample.postgress.bean.DataXceiver;
import com.sample.postgress.mapper.DataXceiverRowMapper;

import java.sql.Connection;
import java.sql.DriverManager;

@Repository

public class DataXceiverDaoImpl implements DataXceiverDao{

	

	
	public DataXceiverDaoImpl(NamedParameterJdbcTemplate template) {  
        this.template = template;  
	}


	NamedParameterJdbcTemplate template;  


	
	@Override
	public List<DataXceiver> findAll() {
		return template.query("select * from blocks natural join destinations natural join common_fields", new DataXceiverRowMapper());
	}

	

	
	@Override
	public void insertMultyCommonFields(List<DataXceiver> dxs) {
		String sql = " INSERT INTO common_fields(id, ipAddress, timeStamp, type)" + "values(?,?,?,?);";
		try{
			Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/LogDB", "postgres", "Fall19");

			PreparedStatement ps = connection.prepareStatement(sql);
			for (DataXceiver dx : dxs) {
		        ps.setInt(1, dx.getId());
				ps.setString(2, dx.getSourceIp());
		        ps.setTimestamp(3, dx.getTimeStamp());
		        ps.setString(4, dx.getType());
		        ps.addBatch();
			}
			ps.executeBatch();
			ps.close();
			connection.close();	
		}
		catch (Exception e) {
	        e.printStackTrace();
		}
	}



	@Override
	public void insertMultyBlocks(List<DataXceiver> dxs) {
		String sql = " INSERT INTO blocks(id, block_id)" + "values(?,?);";
		try{
			Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/LogDB", "postgres", "Fall19");

			PreparedStatement ps = connection.prepareStatement(sql);
			for (DataXceiver dx : dxs) {
		        ps.setInt(1, dx.getId());
		        ps.setString(2, dx.getBlockId());
		        ps.addBatch();
			}
			ps.executeBatch();
			ps.close();
			connection.close();	
		}
		catch (Exception e) {
	        e.printStackTrace();
		}
	}

	
	@Override
	public void insertMultyDestinations(List<DataXceiver> dxs) {
		String sql = " INSERT INTO destinations(id, destination_ip)" + "values(?,?);";
		try{
			Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/LogDB", "postgres", "Fall19");

			PreparedStatement ps = connection.prepareStatement(sql);
			for (DataXceiver dx : dxs) {
		        ps.setInt(1, dx.getId());
		        ps.setString(2, dx.getDestinationIp());
		        ps.addBatch();
			}
			ps.executeBatch();
			ps.close();
			connection.close();	
		}
		catch (Exception e) {
	        e.printStackTrace();
		}
	}

	
	
	
	@Override
	public void insertMultySizes(List<DataXceiver> dxs) {
		String sql = " INSERT INTO sizes(id, size)" + "values(?,?);";
		try{
			Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/LogDB", "postgres", "Fall19");

			PreparedStatement ps = connection.prepareStatement(sql);
			for (DataXceiver dx : dxs) {
		        ps.setInt(1, dx.getId());
		        ps.setInt(2, dx.getSize());
		        ps.addBatch();
			}
			ps.executeBatch();
			ps.close();
			connection.close();	
		}
		catch (Exception e) {
	        e.printStackTrace();
		}
	}

	@Override	
	public void insertMultyRow(List<DataXceiver> dxs) {
		insertMultyCommonFields(dxs);
		insertMultyBlocks(dxs);
		insertMultyDestinations(dxs);
		insertMultySizes(dxs);
	}	
	
	
	@Override
	public void insertCommonFields(DataXceiver dx) {
		 final String sql = "INSERT INTO common_fields(id, ipAddress, timeStamp, type)" + "values(:id, :sourceIp, :timeStamp, :type);";
	        KeyHolder holder = new GeneratedKeyHolder();
	        SqlParameterSource param = new MapSqlParameterSource()
   	        .addValue("id", dx.getId())
   	        .addValue("sourceIp", dx.getSourceIp())
			.addValue("timeStamp", dx.getTimeStamp())
			.addValue("type", dx.getType());
	        template.update(sql,param, holder);
	}

	@Override
	public void insertBlockId(DataXceiver dx) {
		 final String sql = " INSERT INTO blocks(id, block_id)" + "values(:id, :block_id);";

	        KeyHolder holder = new GeneratedKeyHolder();
	        SqlParameterSource param = new MapSqlParameterSource()
   	        .addValue("id", dx.getId())
			.addValue("block_id", dx.getBlockId());
	        template.update(sql,param, holder);
	}

	@Override
	public void insertDestinationIp(DataXceiver dx) {
		 final String sql = " INSERT INTO destinations(id, destination_ip)" + "values(:id, :destination_ip);";

	        KeyHolder holder = new GeneratedKeyHolder();
	        SqlParameterSource param = new MapSqlParameterSource()
  	        .addValue("id", dx.getId())
			.addValue("destination_ip", dx.getBlockId());
	        template.update(sql,param, holder);
	}

	@Override
	public void insertSize(DataXceiver dx) {
		 final String sql = " INSERT INTO sizes(id, size)" + "values(:id, :size);";

	        KeyHolder holder = new GeneratedKeyHolder();
	        SqlParameterSource param = new MapSqlParameterSource()
  	        .addValue("id", dx.getId())
			.addValue("size", dx.getSize());
	        template.update(sql,param, holder);
	}
	@Override 
	public void insertDataXceiver(DataXceiver dx) {
		try{
			insertCommonFields(dx);
			insertBlockId(dx);
			insertDestinationIp(dx);
			insertSize(dx);
		}
		catch (Exception e) {
			System.out.println("Problem with DataXceiver: ");
			dx.printDataXceiver();
			deleteDataXceiver(dx);
		}						
	}
	

	
	@Override
	public void deleteDataXceiver(DataXceiver dx) {
		deleteBlock(dx);
		deleteDestination(dx);
		deleteSize(dx);
		deleteCommonField(dx);
	}

	void deleteBlock(DataXceiver dx) {
		try {
			final String sql = "delete from blocks where id=:id";
				 
	
			 Map<String,Object> map=new HashMap<String,Object>();  
			 map.put("id", dx.getId());
		
			 template.execute(sql,map,new PreparedStatementCallback<Object>() {  
			    @Override  
			    public Object doInPreparedStatement(PreparedStatement ps)  
			            throws SQLException, DataAccessException {  
			        return ps.executeUpdate();  
			    }  
			});  
		}
		catch (Exception e) { }
	}

	void deleteDestination(DataXceiver dx) {	
		try {
			final String sql = "delete from destinations where id=:id";
			 
			
			 Map<String,Object> map=new HashMap<String,Object>();  
			 map.put("id", dx.getId());
		
			 template.execute(sql,map,new PreparedStatementCallback<Object>() {  
			   @Override  
			    public Object doInPreparedStatement(PreparedStatement ps)  
			            throws SQLException, DataAccessException {  
			        return ps.executeUpdate();  
			    }  
			});  

		}
		catch (Exception e) { }
	}

	void deleteSize(DataXceiver dx) {
		try {
			final String sql = "delete from size where id=:id";
					 
					
			Map<String,Object> map=new HashMap<String,Object>();  
			map.put("id", dx.getId());
				
			template.execute(sql,map,new PreparedStatementCallback<Object>() {  
			    @Override  
			    public Object doInPreparedStatement(PreparedStatement ps)  
			            throws SQLException, DataAccessException {  
			        return ps.executeUpdate();  
			    }  
			});  
		}
		catch (Exception e) { }
	}
	
	void deleteCommonField(DataXceiver dx) {
		try {
			final String sql = "delete from common_fields where id=:id";
			 
					
			 Map<String,Object> map=new HashMap<String,Object>();  
			 map.put("id", dx.getId());
		
			 template.execute(sql,map,new PreparedStatementCallback<Object>() {  
			    @Override  
			    public Object doInPreparedStatement(PreparedStatement ps)  
			    		throws SQLException, DataAccessException {  
			    	return ps.executeUpdate();  
				}  
			});  
	
		}
		catch (Exception e) {} 
	}

}
