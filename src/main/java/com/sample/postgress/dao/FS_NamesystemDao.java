package com.sample.postgress.dao;

import java.util.List;

import com.sample.postgress.bean.FS_Namesystem;

public interface FS_NamesystemDao{

	void insertMultyCommonFields(List<FS_Namesystem> fsn);
	void insertMultyBlocks(List<FS_Namesystem> fsn);
	void insertMultyDestinations(List<FS_Namesystem> fsn);
	
	void insertCommonFields(FS_Namesystem fsn);
	void insertBlockId(FS_Namesystem fsn);
	void insertDestinationIp(FS_Namesystem fsn);

	void insertFS_Namesystem(FS_Namesystem fsn);

	void insertMultyRow(List<FS_Namesystem> fsn);
	
	void deleteFS_Namesystem(FS_Namesystem fsn);
}
