package com.sample.postgress.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.sample.postgress.bean.FS_Namesystem;

import java.sql.Connection;
import java.sql.DriverManager;

@Repository

public class FS_NamesystemDaoImpl implements FS_NamesystemDao{

		
	public FS_NamesystemDaoImpl(NamedParameterJdbcTemplate template) {  
        this.template = template;  
	}

	NamedParameterJdbcTemplate template;  

	@Override
	public void insertMultyCommonFields(List<FS_Namesystem> fsns) {
		String sql = " INSERT INTO common_fields(id, ipAddress, timeStamp, type)" + "values(?,?,?,?);";
		try{
			Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/LogDB", "postgres", "Fall19");

			PreparedStatement ps = connection.prepareStatement(sql);
			for (FS_Namesystem fsn : fsns) {
		        ps.setInt(1, fsn.getId());
				ps.setString(2, fsn.getSourceIp());
		        ps.setTimestamp(3, fsn.getTimeStamp());
		        ps.setString(4, fsn.getType());
		        ps.addBatch();
			}
			ps.executeBatch();
			ps.close();
			connection.close();	
		}
		catch (Exception e) {
	        e.printStackTrace();
		}
	}



	@Override
	public void insertMultyBlocks(List<FS_Namesystem> fsns) {
		String sql = " INSERT INTO blocks(id, block_id)" + "values(?,?);";
		try{
			Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/LogDB", "postgres", "Fall19");

			PreparedStatement ps = connection.prepareStatement(sql);
			for (FS_Namesystem fsn : fsns) {
				List<String> list = fsn.getBlockId();
				for (String s : list)
				{
			        ps.setInt(1, fsn.getId());
			        ps.setString(2, s);
			        ps.addBatch();
				}
			}
			ps.executeBatch();
			ps.close();
			connection.close();	
		}
		catch (Exception e) {
	        e.printStackTrace();
		}
	}

	

	@Override
	public void insertMultyDestinations(List<FS_Namesystem> fsns) {
		String sql = " INSERT INTO destinations(id, destination_ip)" + "values(?,?);";
		try{
			Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/LogDB", "postgres", "Fall19");

			PreparedStatement ps = connection.prepareStatement(sql);
			for (FS_Namesystem fsn : fsns) {
				List<String> list = fsn.getDestinationIp();
				for (String s : list)
				{
					if(s!=null) {
				        ps.setInt(1, fsn.getId());
				        ps.setString(2, s);
				        ps.addBatch();
					}
				}
			}
			ps.executeBatch();
			ps.close();
			connection.close();	
		}
		catch (Exception e) {
	        e.printStackTrace();
		}
	}

	
	@Override
	public void insertMultyRow(List<FS_Namesystem> fsns) {
		insertMultyCommonFields(fsns);
		insertMultyBlocks(fsns);
		insertMultyDestinations(fsns);
	}	
	
	
	@Override
	public void insertCommonFields(FS_Namesystem fsn) {
		 final String sql = "INSERT INTO common_fields(id, ipAddress, timeStamp, type)" + "values(:id, :sourceIp, :timeStamp, :type);";
	        KeyHolder holder = new GeneratedKeyHolder();
	        SqlParameterSource param = new MapSqlParameterSource()
   	        .addValue("id", fsn.getId())
   	        .addValue("sourceIp", fsn.getSourceIp())
			.addValue("timeStamp", fsn.getTimeStamp())
			.addValue("type", fsn.getType());
	        template.update(sql,param, holder);
	}

	@Override
	public void insertBlockId(FS_Namesystem fsn) {
		List<String> list = fsn.getBlockId();
		for (String s : list){
			final String sql = " INSERT INTO blocks(id, block_id)" + "values(:id, :block_id);";

			KeyHolder holder = new GeneratedKeyHolder();
	        SqlParameterSource param = new MapSqlParameterSource()
   	        .addValue("id", fsn.getId())
			.addValue("block_id", s);
	        template.update(sql,param, holder);
		}
	}

	@Override
	public void insertDestinationIp(FS_Namesystem fsn) {
		List<String> list = fsn.getDestinationIp();
		for (String s : list)
		{
			if(s!=null)
			{
				final String sql = " INSERT INTO destinations(id, destination_ip)" + "values(:id, :destination_ip);";
		        KeyHolder holder = new GeneratedKeyHolder();
		        SqlParameterSource param = new MapSqlParameterSource()
	  	        .addValue("id", fsn.getId())
				.addValue("destination_ip", s);
		        template.update(sql,param, holder);
			}
		}
	}

	
	@Override 
	public void insertFS_Namesystem(FS_Namesystem fsn) {
		try{
			insertCommonFields(fsn);
			insertBlockId(fsn);
			insertDestinationIp(fsn);
		}
		catch (Exception e) {
			System.out.println("Problem with FS_Namesystem: ");
			deleteFS_Namesystem(fsn);
		}						
	}
	
	
	@Override
	public void deleteFS_Namesystem(FS_Namesystem fsn) {
		deleteBlock(fsn);
		deleteDestination(fsn);
		deleteCommonField(fsn);
	}

	
	void deleteBlock(FS_Namesystem fsn) {
		try {
			final String sql = "delete from blocks where id=:id";
				 
	
			 Map<String,Object> map=new HashMap<String,Object>();  
			 map.put("id", fsn.getId());
		
			 template.execute(sql,map,new PreparedStatementCallback<Object>() {  
			    @Override  
			    public Object doInPreparedStatement(PreparedStatement ps)  
			            throws SQLException, DataAccessException {  
			        return ps.executeUpdate();  
			    }  
			});  
		}
		catch (Exception e) { }
	}

	
	void deleteDestination(FS_Namesystem fsn) {	
		try {
			final String sql = "delete from destinations where id=:id";
			 
			
			 Map<String,Object> map=new HashMap<String,Object>();  
			 map.put("id", fsn.getId());
		
			 template.execute(sql,map,new PreparedStatementCallback<Object>() {  
			   @Override  
			    public Object doInPreparedStatement(PreparedStatement ps)  
			            throws SQLException, DataAccessException {  
			        return ps.executeUpdate();  
			    }  
			});  

		}
		catch (Exception e) { }
	}

	
	void deleteCommonField(FS_Namesystem fsn) {
		try {
			final String sql = "delete from common_fields where id=:id";
			 
					
			 Map<String,Object> map=new HashMap<String,Object>();  
			 map.put("id", fsn.getId());
		
			 template.execute(sql,map,new PreparedStatementCallback<Object>() {  
			    @Override  
			    public Object doInPreparedStatement(PreparedStatement ps)  
			    		throws SQLException, DataAccessException {  
			    	return ps.executeUpdate();  
				}  
			});  
	
		}
		catch (Exception e) {} 
	}

}
