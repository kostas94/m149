package com.sample.postgress.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.sample.postgress.bean.Access;

public class AccessRowMapper implements RowMapper<Access> {

	@Override
	public Access mapRow(ResultSet rs, int arg1) throws SQLException {
		Access acc = new Access();
		acc.setId(rs.getInt("id"));
		acc.setIpAddress(rs.getString("ipAddress"));
		acc.setUserId(rs.getString("userId"));
		acc.setTimeStamp(rs.getTimestamp("timeStamp"));
		acc.setHttpMethod(rs.getString("httpMethod"));
		acc.setResourceRequested(rs.getString("resourceRequested"));
		acc.setHttpResponseStatus(rs.getInt("httpResponseStatus"));
		acc.setResponseSize(rs.getInt("responseSize"));
		acc.setReferer(rs.getString("referer"));
		acc.setUserAgentString(rs.getString("userAgentString"));
		
		return acc;
	}

}