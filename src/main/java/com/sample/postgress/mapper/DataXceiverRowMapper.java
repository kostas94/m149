package com.sample.postgress.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.sample.postgress.bean.DataXceiver;

public class DataXceiverRowMapper implements RowMapper<DataXceiver> {

	@Override
	public DataXceiver mapRow(ResultSet rs, int arg1) throws SQLException {
		DataXceiver dx = new DataXceiver();
		dx.setId(rs.getInt("id"));
		dx.setTimeStamp(rs.getTimestamp("timeStamp"));
		dx.setBlockId(rs.getString("blockId"));
		dx.setSourceIp(rs.getString("sourceIp"));
		dx.setDestinationIp(rs.getString("DestinationIp"));
		dx.setSize(rs.getInt("size"));
		dx.setType(rs.getString("type"));
		
		return dx;
	}

}