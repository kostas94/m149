package com.sample.postgress.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.sample.postgress.bean.FS_Namesystem;

public class FS_NamesystemRowMapper implements RowMapper<FS_Namesystem> {

	@Override
	public FS_Namesystem mapRow(ResultSet rs, int arg1) throws SQLException {
		FS_Namesystem fsn = new FS_Namesystem();
		fsn.setId(rs.getInt("id"));
		fsn.setTimeStamp(rs.getTimestamp("timeStamp"));
		fsn.setBlockId(rs.getString("blockId"));
		fsn.setSourceIp(rs.getString("sourceIp"));
		fsn.setDestinationIp(rs.getString("DestinationIp"));
		fsn.setType(rs.getString("type"));
		
		return fsn;
	}

}