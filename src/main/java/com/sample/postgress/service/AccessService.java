package com.sample.postgress.service;

import java.util.List;

import com.sample.postgress.bean.Access;

public interface AccessService {

	List<Access> findAll();

	List<Access> findOne(Access acc);

	void insertAccess(Access acc);
	
	public boolean existTable();
	
	void insertMultyRow(List<Access> acc);

	void deleteAccess(Access acc);
	
}
