package com.sample.postgress.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.sample.postgress.dao.AccessDao;
import com.sample.postgress.bean.Access;


@Component

public class AccessServiceImpl implements AccessService{

	@Resource 
	AccessDao accessDao;

	@Override
	public List<Access> findAll() {
		return accessDao.findAll();
	}

	@Override
	public boolean existTable() {
		return accessDao.existTable();
	}

	@Override
	public List<Access> findOne(Access acc) {
		return accessDao.findOne(acc);
	}

	@Override
	public void insertMultyRow(List<Access> acc) {
		accessDao.insertMultyRow(acc);
	}
	
	
	@Override
	public void insertAccess(Access acc) {
		accessDao.insertAccess(acc);
		
	}

	@Override
	public void deleteAccess(Access acc) {
		accessDao.deleteAccess(acc);
		
	}
}
