package com.sample.postgress.service;

import java.util.List;

import com.sample.postgress.bean.DataXceiver;

public interface DataXceiverService {
	List<DataXceiver> findAll();

	void insertDataXceiver(DataXceiver dx);
	
	void insertMultyRow(List<DataXceiver> dx);

	void deleteDataXceiver(DataXceiver dx);
	
}
