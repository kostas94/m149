package com.sample.postgress.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.sample.postgress.dao.DataXceiverDao;
import com.sample.postgress.bean.DataXceiver;


@Component

public class DataXceiverServiceImpl implements DataXceiverService{

	@Resource 
	DataXceiverDao dataXceiverDao;

	@Override
	public List<DataXceiver> findAll() {
		return dataXceiverDao.findAll();
	}

	@Override
	public void insertMultyRow(List<DataXceiver> dx) {
		dataXceiverDao.insertMultyRow(dx);
	}
	
	
	@Override
	public void insertDataXceiver(DataXceiver dx) {
		dataXceiverDao.insertDataXceiver(dx);
		
	}

	@Override
	public void deleteDataXceiver(DataXceiver dx) {
		dataXceiverDao.deleteDataXceiver(dx);
		
	}
}
