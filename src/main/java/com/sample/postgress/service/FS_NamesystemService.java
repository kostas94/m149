package com.sample.postgress.service;

import java.util.List;

import com.sample.postgress.bean.FS_Namesystem;

public interface FS_NamesystemService {

	void insertFS_Namesystem(FS_Namesystem fsn);
	
	void insertMultyRow(List<FS_Namesystem> fsn);

	void deleteFS_Namesystem(FS_Namesystem fsn);
	
}
