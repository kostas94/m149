package com.sample.postgress.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.sample.postgress.dao.FS_NamesystemDao;
import com.sample.postgress.bean.FS_Namesystem;


@Component

public class FS_NamesystemServiceImpl implements FS_NamesystemService{

	@Resource 
	FS_NamesystemDao dataXceiverDao;

	@Override
	public void insertMultyRow(List<FS_Namesystem> fsn) {
		dataXceiverDao.insertMultyRow(fsn);
	}
	
	
	@Override
	public void insertFS_Namesystem(FS_Namesystem fsn) {
		dataXceiverDao.insertFS_Namesystem(fsn);
		
	}

	@Override
	public void deleteFS_Namesystem(FS_Namesystem fsn) {
		dataXceiverDao.deleteFS_Namesystem(fsn);
		
	}
}
