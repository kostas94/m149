CREATE TABLE common_fields (
	id INT,
	ipAddress VARCHAR (1024),
	timeStamp timestamp with time zone,
	type VARCHAR (32),
	PRIMARY KEY (id)
);


CREATE TABLE accesslog (
	id INTEGER REFERENCES common_fields(id),
	httpMethod VARCHAR (256),
	httpResponseStatus INT,
	referer VARCHAR (2048),
	resourceRequested VARCHAR (2048),
	responseSize INT DEFAULT -1,
	userAgentString VARCHAR (512),
	userId VARCHAR (1024),
	PRIMARY KEY (id)
);

CREATE TABLE blocks (
	key_id SERIAL PRIMARY KEY,
	id INTEGER REFERENCES common_fields(id),
	block_id VARCHAR (64)
);

CREATE TABLE destinations (
	key_id SERIAL PRIMARY KEY,
	id INTEGER REFERENCES common_fields(id),
	destination_ip VARCHAR (64)
);


CREATE TABLE sizes (
	id INTEGER REFERENCES common_fields(id),
	size INT DEFAULT -1,
	PRIMARY KEY (id)
);

CREATE TABLE users(
	username varchar(20) NOT NULL,
	password varchar(20) NOT NULL,
	firstName varchar(20) NOT NULL,
	lastName varchar(20) NOT NULL,
	userEmail varchar(40) NOT NULL,
	userAddress varchar(60),
	enabled boolean NOT NULL DEFAULT FALSE,
	primary key(username)
);

CREATE TABLE user_roles (
	user_role_id SERIAL PRIMARY KEY,
	username varchar(20) NOT NULL,
	role varchar(20) NOT NULL,
	UNIQUE (username, role),
	FOREIGN KEY (username) REFERENCES users (username)
);

CREATE TABLE history (
	history_id SERIAL PRIMARY KEY,
	username varchar(20) NOT NULL,
	action_description varchar(2048) NOT NULL,
	timeStamp timestamp with time zone,
	FOREIGN KEY (username) REFERENCES users (username)
);

